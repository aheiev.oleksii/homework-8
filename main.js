let getFirstNum, getSecondNum, getStepNum, factorialize;
let sum = 0;
do {
  do {
    getFirstNum = parseInt(prompt("Enter the first num", "1"));
  } while (isNaN(getFirstNum) || getFirstNum === null);

  do {
    getSecondNum = parseInt(prompt("Enter the second num", "10"));
  } while (isNaN(getSecondNum) || getSecondNum === null);
} while (getFirstNum >= getSecondNum);

do {
  getStepNum = parseInt(prompt("Enter the step", "1"));
} while (isNaN(getStepNum) || getStepNum === null);

for (let i = getFirstNum; i <= getSecondNum; i += getStepNum) {
  factorialize = 1;

  console.log(`Start count from ${i}`);

  for (let k = 1; k <= i; k++) {
    factorialize *= k;
  }
  sum += factorialize;

  console.log(`Factorialize of number ${i} is ${factorialize} and sum of factorialize is ${sum}`);
}
